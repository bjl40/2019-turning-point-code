#include "main.h"
#include "motors.hpp"

//rip 6, 11, 15, 13


pros::Motor horiAdjust(17, pros::E_MOTOR_GEARSET_18, false, pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor vertAdjust(20, pros::E_MOTOR_GEARSET_18, true, pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor backLeft(19, pros::E_MOTOR_GEARSET_18, false, pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor backRight(12, pros::E_MOTOR_GEARSET_18, true, pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor frontRight(16, pros::E_MOTOR_GEARSET_18, true, pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor frontLeft(14, pros::E_MOTOR_GEARSET_18, false, pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor BIntake(10, pros::E_MOTOR_GEARSET_18, true, pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor shooter(18, pros::E_MOTOR_GEARSET_18, false, pros::E_MOTOR_ENCODER_COUNTS);

//rip A, B
pros::ADILineSensor bottomBall('C');
pros::ADILineSensor topBall('D');
pros::ADIEncoder mMWheel('G', 'H', true);
pros::ADIEncoder bMWheel('E', 'F', true);
pros::ADIAnalogIn hAngle('B');
pros::ADIAnalogIn vAngle('A');
pros::Controller c1(pros::E_CONTROLLER_MASTER);
pros::Controller c2(pros::E_CONTROLLER_PARTNER);

const double puncherVelocity = 296.66;

//900 ticks per rev.
const double countsPerInch = 70.249724, countsPerDegree = -1, distBetweenWheel = 6.1666, odomCountPerInch = 41.7;
const double distanceBackWheel = 5.0, distanceMiddleWheel = .2;
const double PI = 3.14159265358979323846, g = 386.09;
const int BIL = 1000000000, MIL = 1000000;

//Flag height = 6 in, flag length = 9.9.

const double puncherHeight = 15.4;
/* Team RED!!!!*/
const fTarget flagLocation[3][3] = {
  {fTarget(20.5, 129.9, 15.3), fTarget(65.4, 129.9, 15.3), fTarget(114.2, 129.9, 15.3)},
  {fTarget(20.5, 129.9, 29.4), fTarget(65.4, 129.9, 29.4), fTarget(114.2, 129.9, 29.4)},
  {fTarget(20.5, 129.9, 45.3), fTarget(65.4, 129.9, 45.3), fTarget(114.2, 129.9, 45.3)}
};

/*TEAM BLUE!!!!!*/
/*
const fTarget flagLocation[3][3] = {
  {fTarget(29.5, 129.9, 15.3), fTarget(74.4, 129.9, 15.3), fTarget(125.2, 129.9, 15.3)},
  {fTarget(29.5, 129.9, 29.4), fTarget(74.4, 129.9, 29.4), fTarget(125.2, 129.9, 29.4)},
  {fTarget(29.5, 129.9, 45.3), fTarget(74.4, 129.9, 45.3), fTarget(125.2, 129.9, 45.3)}
};*/

int clamp(int n, int min, int max){return n < min ? min : (n > max ? max : n);}
double clamp (double n, double min, double max){return n < min ? min : (n > max ? max : n);}
int millis(){return pros::millis();}

/////////////Drive Functions/////////////
double getDL(){ return (backLeft.get_position() + frontLeft.get_position())/2;}
double getDR(){ return (backRight.get_position() + frontRight.get_position())/2;}
double getMWheel(){ return mMWheel.get_value();}
double getBWheel(){ return bMWheel.get_value();}
void setDL(int s){
  s = clamp(s, -127, 127);
  backLeft.move(s);
  frontLeft.move(s);
}
void setDR(int s){
  s = clamp(s, -127, 127);
  backRight.move(s);
  frontRight.move(s);
}



/////////////Intake Functions///////////////
void setIntake(int s){
  s = clamp(s, -127, 127);
  BIntake.move(s);
}

double getPuncher(){
  return shooter.get_position();
}

// dist punch = 5.03
// dist adjust = 3
// start angle = 60

///////////Puncher Functions///////////

bool pidPuncher(int target){
  puncherPid.target = target;
  puncherPid.sensVal = getPuncher();
  double power = puncherPid.update();


  if (puncherPid.doneTime <= millis()){
    power = 0;
    shooter.move(clamp(power, 0.0, 127.0));
    return true;
  } else {
    shooter.move(clamp(power, 0.0, 127.0));
    return false;
  }
}


double getHoriAngle(){
  double raw = -1*(hAngle.get_value() - 1909);
  //printf("horiRaw: %f", horiAdjust.get_position());
  return raw*PI/3000;
}
double getVertAngle(){
  int thing = vAngle.get_value();
  double angle;
  angle = .01337*static_cast<double>(thing) + 4.974;

  double rAngle = angle*PI/180;

/*  double gearAngle = -1*(vAngle.get_value() - 3513)*PI/3400 - PI/6;

  Point fixPos = Point(-2.5, 5.25);
  Point gearPos = Point(cos(gearAngle)*1.503, sin(gearAngle)*1.503);
  double c = (fixPos-gearPos).mag(), a = 5.03, b = 3;

  double pAngle = acos((a*a - c*c - b*b)/(-2*b*c));

  double bAngle = atan((gearPos.x-fixPos.x)/(fixPos.y-gearPos.y));

  return .044*PI + PI/2-pAngle-bAngle;*/
  return rAngle;
}

bool pidVert(double target){
  vertPid.doneTime = BIL;
  target = clamp(target, .18666*PI, .29*PI);
  vertPid.target = target;
  vertPid.sensVal = getVertAngle();

  double power = vertPid.update();

  if (isnanf(power)){
    power = -127;
  }

  if (vertPid.sensVal < .18666*PI){
    power = 50;
  }
  vertAdjust.move(-power);


  if (vertPid.doneTime <= millis()){
    return true;
  } else {
    return false;
  }
}

bool pidHori(double target){
  horiPid.doneTime = BIL;
  target = clamp(target, -.22*PI, .185*PI);
  horiPid.target = target;
  horiPid.sensVal = getHoriAngle();

  double power = horiPid.update();
  if (fabs(horiPid.sensVal - horiPid.target) > .12*PI){
    power = clamp(power, -100.0, 100.0);
  } else if (fabs(horiPid.sensVal-horiPid.target) > .008*PI){
  power = clamp(power, -30.0, 30.0);
}
  horiAdjust.move(power);

  if (horiPid.doneTime <= millis()){
    return true;
  } else {
    return false;
  }
}


void setShooter(int n){
  shooter.move(n);
}

void setConstants(){

  mMWheel.reset();
  bMWheel.reset();

  drivePid.kp = 15;
  drivePid.kd = 500;
  drivePid.DONE_ZONE = 1;
  turnPid.kp = 2000;
  turnPid.DONE_ZONE = PI/20;

  vertPid.kp = 800;
  vertPid.ki = 2;
  vertPid.kd = 0;
  vertPid.iActiveZone = BIL;
  vertPid.DONE_ZONE = PI/80;

  horiPid.kp = 700;
  horiPid.ki = .2;
  horiPid.kd = 0;
  horiPid.iActiveZone = BIL;
  horiPid.DONE_ZONE = PI/100;

  puncherPid.kp = 1;
  puncherPid.ki = 0;
  puncherPid.kd = 0;
  puncherPid.DONE_ZONE = 430;

}
