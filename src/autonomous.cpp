#include "main.h"
#include "pid.hpp"
#include "motors.hpp"

void pidNoRatchet(int target){
  puncherPid.target = target;
  puncherPid.sensVal = getPuncher();
  double power = puncherPid.update();
  shooter.move(clamp(power, 0.0, 127.0));
}
/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */

/*
        d8888          888                           d888
       d88888          888                          d8888
      d88P888          888                            888
     d88P 888 888  888 888888  .d88b.  88888b.        888
    d88P  888 888  888 888    d88""88b 888 "88b       888
   d88P   888 888  888 888    888  888 888  888       888
  d8888888888 Y88b 888 Y88b.  Y88..88P 888  888       888
 d88P     888  "Y88888  "Y888  "Y88P"  888  888     8888888

Blue side FRONT square.
*/

void auton1() {
  setConstants();
  int i = 0;
  int j;
  int counter = 0;
  int lift = 700;
  odom.setPos(0, 0, 0);
  int flip = 0;
  odom.trackPos();
  pros::delay(100);
  odom.trackPos();
  odom.setPos(129, 82, PI);
  pidDriveInit(Point(105, 82), 0);
  int puncherTarget = 0;
  int kreth = 0;
  while (true) {

    j = 0;
    printf("X Pos: %f\t Y Pos: %f\t T Pos: %f\n", odom.getPos().x, odom.getPos().y, odom.getT());

    pros::lcd::clear_line(1);
    pros::lcd::clear_line(2);
    pros::lcd::clear_line(3);
    pros::lcd::print(1, "X Pos: %f", odom.getPos().x);
    pros::lcd::print(2, "y Pos: %f", odom.getPos().y);
    pros::lcd::print(3, "T Pos: %f PI", odom.getT()/PI);
    pros::lcd::print(4, "Turn target: %f PI", turnPid.target/PI);

    odom.trackPos();

    if (i == j++){//drive forward
      pidNoRatchet(puncherTarget);
      setIntake(0);
      if (pidDrive(100)){
        pidDriveInit(Point(95, 82), 0);
        i++;
        kreth = topBall.get_value();
        counter = 0;
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setIntake(0);
      if (pidDrive(50)){
        pidDriveInit(Point(127, 82), 0);
        i++;
        counter = 0;
      }
    } else if (i == j++){ // drive back
      counter++;
      if (counter < 90){
        setIntake(127);
      } else {
        setIntake(-127);
      }
      if (pidDrive(100)){
        pidFaceInit(Point(123, 104), 200);
        counter = 0;
        i++;
      }
    } else if (i == j++){
      pros::lcd::print(5, "Turn realTarget: %f", turnPid.target);
      setIntake(-127);
      if (pidFace(false, 0)){
        i++;
      }
      faceFlag(fTarget(flagLocation[2][2].x-4, flagLocation[2][2].y, flagLocation[2][2].z));
      //if (fabs(odom.getT()-.5*PI) < .05*PI){//Face Flags and target
        //i++;
      //}
    } else if (i == j++){
      setDL(0);
      setDR(0);//Target Flag
      if (faceFlag(fTarget(flagLocation[2][2].x-4, flagLocation[2][2].y, flagLocation[2][2].z))){
        i++;
        counter = 0;
        puncherTarget += 2700;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){//Punch high
      counter++;
      faceFlag(fTarget(flagLocation[2][2].x-4, flagLocation[2][2].y, flagLocation[2][2].z));
      if (counter > 45){
        pidNoRatchet(puncherTarget);
      }
      if (counter > 65){
        setIntake(127);
      }
      if (counter > 70){
        pidDriveInit(Point(123, 104), 200);
        i++;
        counter = 0;
      }
    } else if (i == j++){//Drive to mid flag position
      counter++;
      setIntake(127);
      pidNoRatchet(puncherTarget);
      if (counter > 50){
        faceFlag(fTarget(flagLocation[1][2].x-2, flagLocation[1][2].y, flagLocation[1][2].z));
      } else {
        faceFlag(flagLocation[2][2]);
      }
      if (pidDrive(100)){
        i++;
      }
    } else if (i == j++){ // Aim for mid flag
      setIntake(127);
      pidNoRatchet(puncherTarget);
      setDL(0);
      setDR(0);
      if(faceFlag(fTarget(flagLocation[1][2].x-2, flagLocation[1][2].y, flagLocation[1][2].z))){
        i++;
        counter = 0;
        puncherTarget += 2700;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){ // shoot mid flag
      setIntake(127);
      counter++;
      if (counter > 75){
        pidNoRatchet(puncherTarget);
      }
      faceFlag(fTarget(flagLocation[1][2].x-2, flagLocation[1][2].y, flagLocation[1][2].z));
      if (counter > 90){
        pidFaceInit(Point(flagLocation[2][2].x+3, flagLocation[2][2].y+2), 200);
        i++;
        counter = 0;
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setIntake(127);
      if (pidFace(false, 0)){
        i++;
        pidDriveInit(Point(flagLocation[2][2].x+3, flagLocation[2][2].y+2), 200);
      }
    } else if (i == j++){ // Drive inwards
      pidNoRatchet(puncherTarget);
      counter++;
      setIntake(127);
      if (pidDrive(100)){
        i++;
        counter = 0;
        pidFaceInit(Point(flagLocation[2][1].x, flagLocation[2][1].y), 200);
      }
    }/* else if (i == j++){
      setIntake(127);
      if (pidFace(false, 0)){
        i++;
        counter = 0;
        //pidDriveInit(Point(106, 99.5), 200);
      }
    } else if (i == j++){ // Drive forward and flip caps
    */  /*counter++;
      if (counter < 175){
        setIntake(-127);
      } else {
        setIntake(0);
      }
      if (pidDrive(40)){
        i++;
        pidDriveInit(Point(118, 99.5), 200);
      }
    } else if (i == j++){
      if (pidDrive(100)){
        i++;
        pidFaceInit(Point(98,120), 200);
        //pidFaceInit(Point(flagLocation[2][1].x-4, flagLocation[2][1].y+4), 200);
      }*/
    else if (i == j++){//Turn and face middle flags
      if (pidFace(false, 0)){
        counter = 0;
        i++;
      }
    } else if (i == j++){//check intake for balls and drive back a little bit
      if (topBall.get_value() > kreth + 50){
        setIntake(127);
      } else {
        setIntake(-127);
      }
      counter++;
      //setDL(-100);
      //setDR(-100);
      faceFlag(fTarget(flagLocation[2][1].x, flagLocation[2][1].y+4, flagLocation[2][1].z+3));
      if (counter > 25){
        i++;
      }
    } else if (i == j++){ // face middle high flag
      if (topBall.get_value() > kreth + 50){
        setIntake(127);
      } else {
        setIntake(-127);
      }
      setDL(0);
      setDR(0);
      if (faceFlag(fTarget(flagLocation[2][1].x, flagLocation[2][1].y+4, flagLocation[2][1].z+3))){
        i++;
        counter = 0;
        puncherTarget += 2700;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){ // shoot middle high flag
      counter++;
      if (counter > 65){
        pidNoRatchet(puncherTarget);
      }
      faceFlag(fTarget(flagLocation[2][1].x, flagLocation[2][1].y+4, flagLocation[2][1].z+3));
      if (counter > 140){
        i++;
        counter = 0;
      }
    }else if (i == j++){//drive forward
      pidNoRatchet(puncherTarget);
      setIntake(127);
      setDL(100);
      setDR(100);
      faceFlag(fTarget(flagLocation[1][1].x, flagLocation[1][1].y+4, flagLocation[1][1].z+3));
      counter++;
      if (counter > 120){
        i++;
        counter = 0;
      }
    } /*else if (i == j++){ // face middle middle
      pidNoRatchet(puncherTarget);
      setIntake(127);
      setDL(-10);
      setDR(-10);
      if (faceFlag(fTarget(flagLocation[1][1].x+2, flagLocation[1][1].y+4, flagLocation[1][1].z))){
        i++;
        counter = 0;
        puncherTarget+= 2700;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){//shoot middle middle
      counter++;
      if (counter > 95){
        pidNoRatchet(puncherTarget);
      }
       faceFlag(flagLocation[1][1]);
       if (counter > 140){
         counter = 0;
         i++;
       }
    } /*else if (i == j++){//turn to toggle low flag
      pidNoRatchet(puncherTarget);
      setDL(100);
      setDR(30);
      counter++;
      if (counter > 70){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      setDL(100);
      setDR(100);
      counter++;
      if (counter > 100){
        i++;
      }
    } */else if (i == j++){
    pidNoRatchet(puncherTarget);
      setDL(100);
      setDR(30);
      counter++;
      if (counter > 50){
        i++;

      }
    }
    /*(else if (i == j++){
      if (pidFace(false, 0)){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      setIntake(-127);
      setDL(100);
      setDR(100);
      counter++;
      if (counter > 90){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      counter++;
      setDL(100);
      setDR(30);
      setIntake(0);
      if (counter > 50){
        i++;
      }
    }*/ else if (i == j++){
      setDL(0);
      setDR(0);
    }
    pros::delay(10);
  }
}

/*
        d8888          888                           .d8888b.
       d88888          888                          d88P  Y88b
      d88P888          888                                 888
     d88P 888 888  888 888888  .d88b.  88888b.           .d88P
    d88P  888 888  888 888    d88""88b 888 "88b      .od888P"
   d88P   888 888  888 888    888  888 888  888     d88P"
  d8888888888 Y88b 888 Y88b.  Y88..88P 888  888     888"
 d88P     888  "Y88888  "Y888  "Y88P"  888  888     888888888

  red side FRONT square
*/

void auton2() {
  setConstants();
  int i = 0;
  int j;
  int counter = 0;
  int lift = 700;
  odom.setPos(0, 0, 0);
  int flip = 0;
  odom.trackPos();
  pros::delay(100);
  odom.trackPos();
  odom.setPos(13, 82, 0);
  pidDriveInit(Point(43, 82), 0);
  int puncherTarget = 0;
  int kreth = 0;
  while (true) {

    j = 0;
    printf("X Pos: %f\t Y Pos: %f\t T Pos: %f\n", odom.getPos().x, odom.getPos().y, odom.getT());

    pros::lcd::clear_line(1);
    pros::lcd::clear_line(2);
    pros::lcd::clear_line(3);
    pros::lcd::print(1, "X Pos: %f", odom.getPos().x);
    pros::lcd::print(2, "y Pos: %f", odom.getPos().y);
    pros::lcd::print(3, "T Pos: %f PI", odom.getT()/PI);

    odom.trackPos();

    if (i == j++){//drive forward
      pidNoRatchet(puncherTarget);
      setIntake(0);
      if (pidDrive(100)){

        pidDriveInit(Point(48, 82), 200);
        i++;
        kreth = topBall.get_value();
        counter = 0;
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      if (pidDrive(50)){
        pidDriveInit(Point(19, 82), 200);
        i++;
      }
    }else if (i == j++){ // drive back
      counter++;
      if (counter < 90){
        setIntake(127);
      } else {
        setIntake(-127);
      }
      if (pidDrive(100)){
        pidFaceInit(Point(flagLocation[2][0].x+2, 104), 200); //Init Turn to Face Flags
        i++;
      }
    } else if (i == j++){
      setIntake(-127);
      faceFlag(fTarget(flagLocation[2][0].x-3, flagLocation[2][0].y, flagLocation[2][0].z));
      if (pidFace(false, 0)){//Face Flags and target
        i++;
      }
    } else if (i == j++){
      setDL(0);
      setDR(0);//Target Flag
      if (faceFlag(fTarget(flagLocation[2][0].x-3, flagLocation[2][0].y, flagLocation[2][0].z))){
        i++;
        counter = 0;
        puncherTarget += 2700;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){//Punch high
      counter++;
      faceFlag(fTarget(flagLocation[2][0].x-3, flagLocation[2][0].y, flagLocation[2][0].z));
      if (counter > 75){
        pidNoRatchet(puncherTarget);
      }
      if (counter > 85){
        setIntake(127);
      }
      if (counter > 120){
        pidDriveInit(Point(flagLocation[2][0].x+2, 104), 200);
        i++;
        counter = 0;
      }
    } else if (i == j++){//Drive to mid flag position
      counter++;
      setIntake(127);
      pidNoRatchet(puncherTarget);
      if (counter > 50){
        faceFlag(fTarget(flagLocation[1][0].x-3, flagLocation[1][0].y, flagLocation[1][0].z));
      } else {
        faceFlag(flagLocation[2][0]);
      }
      if (pidDrive(100)){
        i++;
      }
    } else if (i == j++){ // Aim for mid flag
      setIntake(127);
      pidNoRatchet(puncherTarget);
      setDL(0);
      setDR(0);
      if(faceFlag(fTarget(flagLocation[1][0].x-3, flagLocation[1][0].y, flagLocation[1][0].z))){
        i++;
        counter = 0;
        puncherTarget += 2700;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){ // shoot mid flag
      setIntake(127);
      counter++;
      if (counter > 65){
        pidNoRatchet(puncherTarget);
      }
      faceFlag(fTarget(flagLocation[1][0].x-3, flagLocation[1][0].y, flagLocation[1][0].z));
      if (counter > 90){
        pidFaceInit(Point(flagLocation[2][0].x-9, flagLocation[2][0].y+2), 200);
        i++;
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setIntake(127);
      if (pidFace(false, 0)){
        i++;
        pidDriveInit(Point(flagLocation[2][0].x-9, flagLocation[2][0].y+2), 200);
      }
    } else if (i == j++){ // Drive inwards
      pidNoRatchet(puncherTarget);
      setIntake(127);
      if (pidDrive(100)){
        i++;
        odom.setPos(17, 128, PI/2);
        counter = 0;
        pidDriveInit(Point(17, 108), 200);
      }
    } else if (i == j++){//DriveBackwards
      setIntake(127);
      if (pidDrive(100)){
        i++;/*
        counter = 0;
        pidDriveInit(Point(30, 90), 200);
      }
    } else if (i == j++){ // Drive forward and flip caps
      counter++;
      if (counter < 175){
        setIntake(-127);
      } else {
        setIntake(0);
      }
      if (pidDrive(40)){
        i++;
        pidDriveInit(Point(30, 90), 0);
      }
    } else if (i == j++){
      if (pidDrive(100)){
        i++;*/
        pidFaceInit(Point(flagLocation[2][1].x+46, flagLocation[2][1].y), 200);
      }
    }else if (i == j++){//Turn and face middle flags
      if (pidFace(false, 0)){
        counter = 0;
        i++;
      }
    } else if (i == j++){//check intake for balls and drive back a little bit
      if (topBall.get_value() > kreth + 50){
        setIntake(127);
      } else {
        setIntake(-127);
      }
      counter++;
      //setDL(-100);
      //setDR(-100);
      faceFlag(fTarget(flagLocation[2][1].x+46, flagLocation[2][1].y+4, flagLocation[2][1].z+7));
      if (counter > 25){
        i++;
      }
    } else if (i == j++){ // face middle high flag
      if (topBall.get_value() > kreth + 50){
        setIntake(127);
      } else {
        setIntake(-127);
      }
      setDL(0);
      setDR(0);
      if (faceFlag(fTarget(flagLocation[2][1].x+46, flagLocation[2][1].y+4, flagLocation[2][1].z+7))){
        i++;
        counter = 0;
        puncherTarget += 2700;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){ // shoot middle high flag
      counter++;
      if (counter > 55){
        pidNoRatchet(puncherTarget);
      }
      faceFlag(fTarget(flagLocation[2][1].x+46, flagLocation[2][1].y+4, flagLocation[2][1].z+7));
      if (counter > 100){
        counter = 0;
        i++;
      }
    } /*else if (i == j++){//drive forward
      pidNoRatchet(puncherTarget);
      setIntake(127);
      setDL(100);
      setDR(100);
      //faceFlag(fTarget(flagLocation[1][1].x-2, flagLocation[1][1].y+4, flagLocation[1][1].z));
      counter++;
      if (counter > 120){
        i++;
      }
    } /*else if (i == j++){ // face middle middle
      pidNoRatchet(puncherTarget);
      setIntake(127);
      setDL(-1);
      setDR(-1);
      if (faceFlag(fTarget(flagLocation[1][1].x-2, flagLocation[1][1].y+4, flagLocation[1][1].z))){
        i++;
        counter = 0;
        puncherTarget+= 2700;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){//shoot middle middle
      counter++;
      if (counter > 95){
        pidNoRatchet(puncherTarget);
      }
       faceFlag(flagLocation[1][1]);
       if (counter > 140){
         counter = 0;
         i++;
       }
    }*//* else if (i == j++){//turn to toggle low flag
      pidNoRatchet(puncherTarget);
      setDL(30);
      setDR(100);
      counter++;
      if (counter > 40){
        i++;
        counter = 0;
      }
    }*/ /*else if (i == j++){
      pidNoRatchet(puncherTarget);
      setDL(100);
      setDR(100);
      counter++;
      if (counter > 100){
        i++;
      }
    } */else if (i == j++){
      setDL(0);
      setDR(0);
    }/*
    else if (i == j++){
      pidNoRatchet(puncherTarget);
      if (pidDrive(100)){
        pidFaceInit(Point(64, 120), 200);
        i++;
      }
    } else if (i == j++){
      if (pidFace(false, 0)){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      setDL(100);
      setDR(100);
      counter++;
      if (counter > 70){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      setDR(100);
      setDL(40);
      counter++;
      if (counter > 50){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      setDL(0);
      setDR(0);
    }*/
    pros::delay(10);
  }
}
/*
       d8888          888                           .d8888b.
      d88888          888                          d88P  Y88b
     d88P888          888                               .d88P
    d88P 888 888  888 888888  .d88b.  88888b.          8888"
   d88P  888 888  888 888    d88""88b 888 "88b          "Y8b.
  d88P   888 888  888 888    888  888 888  888     888    888
 d8888888888 Y88b 888 Y88b.  Y88..88P 888  888     Y88b  d88P
d88P     888  "Y88888  "Y888  "Y88P"  888  888      "Y8888P"
  Back blue
*/
void auton3(){
  setConstants();
  const int wait = 200;
  int i = 0;
  int j;
  int counter = 0;
  int flip = 0;
  odom.trackPos();
  pros::delay(100);
  odom.trackPos();
  odom.setPos(131, 36, PI);
  pidDriveInit(Point(103, 36), 0);
  int puncherTarget = 0;
  while (true) {

    j = 0;
    printf("X Pos: %f\t Y Pos: %f\t T Pos: %f\n", odom.getPos().x, odom.getPos().y, odom.getT());

    pros::lcd::clear_line(1);
    pros::lcd::clear_line(2);
    pros::lcd::clear_line(3);
    pros::lcd::print(1, "X Pos: %f", odom.getPos().x);
    pros::lcd::print(2, "y Pos: %f", odom.getPos().y);
    pros::lcd::print(3, "T Pos: %f PI", odom.getT()/PI);

    if (i == j++){
      setIntake(0);
      if (pidDrive(127)){
        i++;
        pidDriveInit(Point(97, 36), 0);

      }
    }
    else if (i == j++){
      setIntake(0);
      if (pidDrive(50)){
        i++;
        pidDriveInit(Point(105, 36), 0);
      }
    } else if (i == j++){
      setIntake(127);
      if (pidDrive(100)){
        i++;/*
        pidFaceInit(Point(90, 12), 200);
      }
    }
      else if (i == j++){
        setIntake(127);
      if (pidFace(false, 0)){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      counter++;
      setIntake(127);
      setDL(100);
      setDR(100);
      if (counter > 20){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      setIntake(-127);
      counter++;
      setDL(50);
      setDR(50);
      setIntake(-127);
      if (counter > 100){
        i++;*/
        pidFaceInit(Point(97, 24), 200);
      }
    } else if (i == j++){
      if (pidFace(true, 0)){
        pidDriveInit(Point(97, 24), 200);
        i++;
      }
    } else if (i == j++){
      if (pidDrive(100)){
        pidTurnInit(1*PI/2, 200);
        i++;
      }
    } else if (i == j++){
      if (pidTurn()){
        counter = 0;
        i++;
      }
    } else if (i == j++){
      setDL(80);
      setDR(80);
      counter++;
      if (counter > 80){
        i++;

      }
    } else if (i == j++){
      setDL(0);
      setDR(0);
      counter++;
      if (counter == 50){
        odom.setPos(100, 37, PI/2);
      }
      faceFlag(fTarget(flagLocation[1][0].x-2, flagLocation[1][0].y, flagLocation[1][0].z-3.5));
      if (counter > 300){
        i++;
        puncherTarget += 2700;
        puncherPid.doneTime = BIL;
        counter = 0;
      }
    } else if (i == j++){
      faceFlag(fTarget(flagLocation[1][0].x-2, flagLocation[1][0].y, flagLocation[1][0].z-3.5));
      pidNoRatchet(puncherTarget);
      counter++;
      if (counter > 100){
        i++;
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setIntake(127);
      if (faceFlag(fTarget(flagLocation[2][1].x+16, flagLocation[2][1].y, flagLocation[2][1].z+2))){
        i++;
        counter = 0;
        puncherTarget += 2800;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){
      setIntake(127);
      faceFlag(fTarget(flagLocation[2][1].x+16, flagLocation[2][1].y, flagLocation[2][1].z+2));
      if (counter > 150){
        pidNoRatchet(puncherTarget);
      }
      counter++;
      if (counter > 250){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setDL(100);
      setDR(100);
      counter++;
      if (counter > 90){
        i++;
        counter = 0;
        odom.setPos(0, 0, PI/2);
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setDL(-10);
      setDR(-10);
      counter++;
      if (counter > 8){
        i++;
      }
    } else if (i == j++){
      setDL(0);
      setDR(0);
    }
    odom.trackPos();

    pros::delay(10);
  }
}



/*

        d8888          888                              d8888
       d88888          888                             d8P888
      d88P888          888                            d8P 888
     d88P 888 888  888 888888  .d88b.  88888b.       d8P  888
    d88P  888 888  888 888    d88""88b 888 "88b     d88   888
   d88P   888 888  888 888    888  888 888  888     8888888888
  d8888888888 Y88b 888 Y88b.  Y88..88P 888  888           888
 d88P     888  "Y88888  "Y888  "Y88P"  888  888           888
 Back red
*/

void auton4(){
  setConstants();
  const int wait = 200;
  pidDriveInit(Point(35, 0), 0);
  int i = 0;
  int j;
  int counter = 0;
  int flip = 0;
  odom.trackPos();
  pros::delay(100);
  odom.trackPos();
  odom.setPos(13, 36, 0);
  pidDriveInit(Point(43, 36), 0);
  int puncherTarget = 0;
  while (true) {

    j = 0;
    printf("X Pos: %f\t Y Pos: %f\t T Pos: %f\n", odom.getPos().x, odom.getPos().y, odom.getT());

    pros::lcd::clear_line(1);
    pros::lcd::clear_line(2);
    pros::lcd::clear_line(3);
    pros::lcd::print(1, "X Pos: %f", odom.getPos().x);
    pros::lcd::print(2, "y Pos: %f", odom.getPos().y);
    pros::lcd::print(3, "T Pos: %f PI", odom.getT()/PI);

    if (i == j++){
      setIntake(0);
      if (pidDrive(127)){
        i++;
        pidDriveInit(Point(48, 36), 0);

      }
    }else if( i == j++){
      setIntake(0);
      if (pidDrive(50)){
        pidDriveInit(Point(40, 36), 0);

        i++;
      }
    } else if (i == j++){
      setIntake(127);
      if (pidDrive(100)){
        i++;
        /*pidFaceInit(Point(54, 12), 200);
      }
    }
      else if (i == j++){
      if (pidFace(false, 1)){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      counter++;
      setIntake(-127);
      setDL(100);
      setDR(100);
      if (counter > 20){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      counter++;
      setDL(50);
      setDR(50);
      setIntake(-127);
      if (counter > 100){
        i++;*/
        pidFaceInit(Point(51, 24), 200);
      }
    } else if (i == j++){
      setIntake(-127);
      if (pidFace(true, 0)){
        pidDriveInit(Point(51, 24), 200);
        i++;
      }
    } else if (i == j++){
      setIntake(-127);
      if (pidDrive(100)){
        pidTurnInit(1*PI/2, 200);
        i++;
      }
    } else if (i == j++){
      if (pidTurn()){
        counter = 0;
        i++;
      }
    } else if (i == j++){
      setDL(80);
      setDR(80);
      counter++;
      if (counter > 80){
        i++;
        horiPid.kp = 300;
        odom.setPos(52, 37, PI/2);
      }
    } else if (i == j++){
      setDL(0);
      setDR(0);
      counter++;
      faceFlag(fTarget(flagLocation[1][2].x-2, flagLocation[1][2].y, flagLocation[1][2].z));
      if (counter > 300){
        i++;
        puncherTarget += 2700;
        puncherPid.doneTime = BIL;
        counter = 0;
      }
    } else if (i == j++){
      faceFlag(fTarget(flagLocation[1][2].x-2, flagLocation[1][2].y, flagLocation[1][2].z));
      pidNoRatchet(puncherTarget);
      counter++;
      if (counter > 100){
        i++;
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setIntake(127);
      if (faceFlag(fTarget(flagLocation[2][1].x, flagLocation[2][1].y, flagLocation[2][1].z+4))){
        i++;
        counter = 0;
        puncherTarget += 2700;
        puncherPid.doneTime = BIL;
      }
    } else if (i == j++){
      setIntake(127);
      faceFlag(fTarget(flagLocation[2][1].x, flagLocation[2][1].y, flagLocation[2][1].z+4));
      if (counter > 250){
        pidNoRatchet(puncherTarget);
      }
      counter++;
      if (counter > 350){
        i++;
        counter = 0;
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setDL(100);
      setDR(100);
      counter++;
      if (counter > 80){
        i++;
        counter = 0;
        odom.setPos(0, 0, PI/2);
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setDL(-10);
      setDR(-10);
      counter++;
      if (counter > 8){
        i++;
      }
    } else if (i == j++){
      pidNoRatchet(puncherTarget);
      setDL(0);
      setDR(0);
    }
    odom.trackPos();

    pros::delay(10);
  }
}


/*
 .d8888b.  888      d8b 888 888                     d8888          888
d88P  Y88b 888      Y8P 888 888                    d88888          888
Y88b.      888          888 888                   d88P888          888
 "Y888b.   888  888 888 888 888 .d8888b          d88P 888 888  888 888888  .d88b.  88888b.
    "Y88b. 888 .88P 888 888 888 88K             d88P  888 888  888 888    d88""88b 888 "88b
      "888 888888K  888 888 888 "Y8888b.       d88P   888 888  888 888    888  888 888  888
Y88b  d88P 888 "88b 888 888 888      X88      d8888888888 Y88b 888 Y88b.  Y88..88P 888  888
 "Y8888P"  888  888 888 888 888  88888P'     d88P     888  "Y88888  "Y888  "Y88P"  888  888


  red side back square
*/

void skillsAuton(){
  setConstants();
  const int wait = 500;
  int i = 0;
  int j;
  int counter = 0;
  int lift = 700;
  odom.setPos(13, 36, 0);
  pidDriveInit(Point(51, 36), 0);
  setIntake(127);
  while (true) {
    if (i == j++){}

    odom.trackPos();
    pros::delay(10);
  }

}



void autonomous(){
  auton4();
}
