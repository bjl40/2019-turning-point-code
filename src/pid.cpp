#include "main.h"
#include "motors.hpp"
#include "pid.hpp"
PID DLPid, DRPid, drivePid, turnPid, curvePid;
PID horiPid, vertPid, puncherPid;
Odom odom;

fTarget::fTarget(){
  x = y = z = 0;
}
fTarget::fTarget(double _x, double _y, double _z){
  x = _x, y = _y, z = _z;
}
void fTarget::setFTarget(double _x, double _y, double _z){
  x = _x, y = _y, z = _z;
}
double fTarget::mag() const{return sqrt(pow(x, 2.0) + pow(y, 2.0));}
double fTarget::getZ() {return z;}

Point::Point(){
  x = y = 0;
}

Point::Point(double _x, double _y){
  this->x = _x;
  this->y = _y;
}

void Point::setPoint (double _x, double _y){
  this->x = _x;
  this->y = _y;
}

Point operator+(const Point& p1, const Point& p2) { return Point(p1.x + p2.x, p1.y + p2.y); }
Point operator-(const Point& p1, const Point& p2) { return Point(p1.x - p2.x, p1.y - p2.y); }
double operator*(const Point& p1, const Point& p2) { return p1.x * p2.x + p1.y * p2.y; }
bool operator<(const Point& p1, const Point& p2) {
    Point p1RotCCW = p1.rotate(1);
    if (p1RotCCW * p2 > 0.001) return true;
    return false;
}
bool operator>(const Point& p1, const Point& p2) {
    Point p1RotCCW = p1.rotate(1);
    if (p1RotCCW * p2 < -0.001) return true;
    return false;
}

double Point::mag() const { return sqrt(pow(x, 2.0) + pow(y, 2.0)); }
Point Point::rotate(int dir) const {
    if (dir > 0) {
        return Point(-y, x);
    } else {
        return Point(y, -x);
    }
}
Point Point::abs() {
    Point p(fabs(x), fabs(y));
    return p;
}

/////////////PID
PID::PID(){
  maxIntegral = 9999999;
  DONE_ZONE = 10;
  doneTime = BIL;
  unwind = sensVal = prevSensVal = target = prevErr = errTot = iActiveZone = kp = ki = kd = deriv = 0.0;
  prevTime = prevDUpdateTime = 0;
}

double PID::update(){
  int dt = millis() - prevTime;
    if (dt > 1000) dt = 0;
    prevTime = millis();
    // PROPORTIONAL
    double err = target - sensVal;
    double p = err * kp;
    // DERIVATIVE
    double d = deriv;  // set d to old derivative
    double derivativeDt = millis() - prevDUpdateTime;
    if (derivativeDt > 1000) {
        prevSensVal = sensVal;
        prevDUpdateTime = millis();
    } else if (derivativeDt >= 15) {
        d = ((prevSensVal - sensVal) * kd) / derivativeDt;
        prevDUpdateTime = millis();
        deriv = d;  // save new derivative
        prevSensVal = sensVal;
    }
    // INTEGRAL - Only for lift really.
    errTot += err * dt;
    if (fabs(err) > iActiveZone) errTot = 0;
    if (fabs(d) > 10) {
        double maxErrTot = maxIntegral / ki;
        if (errTot > maxErrTot) errTot = maxErrTot;
        if (errTot < -maxErrTot) errTot = -maxErrTot;
    }
    if ((err > 0.0 && errTot < 0.0) || (err < 0.0 && errTot > 0.0) || fabs(err) < 0.001) {
        if (fabs(err) - unwind > -0.001) {
            errTot = 0.0;
            // printf("UNWIND\n");
        }
    }
    if (fabs(unwind) < 0.001 && fabs(err) < 0.001) {
        errTot = 0.0;
        // printf("UNWIND\n");
    }
    double i = errTot * ki;
    prevErr = err;
    if (fabs(err) <= DONE_ZONE && doneTime > millis()) {
        errTot = 0;
        doneTime = millis();
        printf("DONE\n");
    }
    return p + i + d;
}

Point g_target;
namespace driveData{
Point target;
int wait;
void init (Point t, int w){
  target = t;
  wait = w;
}
}
void pidDriveInit(Point target, const int wait){
  drivePid.doneTime = BIL;
  turnPid.doneTime = BIL;
  driveData::init(target, wait);
}
bool faceFlag(fTarget startTarget){
  double distToFlag = (Point(startTarget.x, startTarget.y)-odom.getPos()).mag();
  double changeHeight = startTarget.z-puncherHeight;

//g is positive
  double vert1 = atan((pow(puncherVelocity, 2)+sqrt(pow(puncherVelocity, 4)-g*(g*pow(distToFlag, 2)+2*changeHeight*pow(puncherVelocity, 2))))/g/distToFlag);
  double vert = atan((pow(puncherVelocity, 2)-sqrt(pow(puncherVelocity, 4)-g*(g*pow(distToFlag, 2)+2*changeHeight*pow(puncherVelocity, 2))))/g/distToFlag);
  double hori  = atan((startTarget.y-odom.y)/(startTarget.x-odom.x));
  double checkHori = asin((startTarget.y-odom.y)/distToFlag);
  if (fabs(hori-checkHori) > .02*PI){hori += PI;}
  if ((startTarget.x-odom.x) == 0){
    hori = PI/2;
  }
  hori -= odom.getT();
  int numIn;
  if (hori < 0){numIn = static_cast<int>(hori/2/PI-.5);}
  else if (hori > 0){numIn = static_cast<int>(hori/2/PI+.5);}
  hori -= numIn*2*PI;

  pidVert(vert);
  pidHori(hori);
  if (fabs(vertPid.sensVal - vert) <= PI/50 && fabs(horiPid.sensVal-hori) <= PI/100){return true;}
  else{return false;}
  /*
  odom.vy*tInAir + puncherVelocity*cos(vert)*cos(hori) = startTarget.y-odom.y;
  odom.vx*tInAir + puncherVelocity*cos(vert)*sin(hori) = startTarget.x-odom.x;
  1/2*9.8*tInAir*tInAir + puncherVelocity*sin(vert)*tInAir = startTarget.z-puncherHeight;*/
}


bool pidDrive(int speed){
  using driveData::target;
  using driveData::wait;
  g_target = target;
  Point pos(odom.getPos());
  static int prevT = 0;
  static Point prevPos(0, 0);
  int dt = millis() - prevT;
  bool returnVal = false;
  if (dt < 100) {
        // Point dir = pos - prevPos;
        Point targetDir = target - pos;
        if (targetDir.mag() < 0.001) {
            setDL(0);
            setDR(0);
        } else {
            // error detection
            Point dirOrientation(cos(odom.getT()), sin(odom.getT()));
            double aErr = acos(clamp((dirOrientation * targetDir) / (dirOrientation.mag() * targetDir.mag()), -1.0, 1.0));

            // allow for driving backwards
            int driveDir = 1;
            if (aErr > PI / 2) {
                driveDir = -1;
                aErr = PI - aErr;
            }
            if (dirOrientation < targetDir) aErr *= -driveDir;
            if (dirOrientation > targetDir) aErr *= driveDir;

            // error correction
            double curA = odom.getT();
            drivePid.target = 0.0;
            drivePid.sensVal = targetDir.mag() * cos(aErr);
            if (drivePid.sensVal < 4) aErr = 0;
            turnPid.target = 0;
            turnPid.sensVal = aErr;
            int turnPwr = clamp((int)turnPid.update(), -85, 85);
            int drivePwr = clamp((int)drivePid.update(), -speed, speed);
            printf("Drive Power: %d       Turn Power: %d\n", drivePwr, turnPwr);
            // prevent turn saturation
            // if (abs(turnPwr) > 0.2 * abs(drivePwr)) turnPwr = (turnPwr < 0 ? -1 : 1) * 0.2 * abs(drivePwr);
            setDL(-drivePwr * driveDir - turnPwr);
            setDR(-drivePwr * driveDir + turnPwr);
        }
        if (drivePid.doneTime + wait < millis()) returnVal = true;
    }
    prevPos = pos;
    prevT = millis();
    return returnVal;
}

int g_pidTurnLimit = 85;
namespace turnData {
double angle;
int wait;
Point face;
void init(double a, int w) {
    angle = a;
    wait = w;
}
void faceInit(Point p, int w){
  wait = w;
  face = p;
}
}
void pidTurnInit(const double angle, const int wait) {
    turnData::init(angle, wait);
    turnPid.doneTime = BIL;
}
void pidFaceInit(Point target, const int wait){
  turnData::faceInit(target, wait);
  turnPid.doneTime = BIL;
}


bool pidTurn() {
    using turnData::angle;
    using turnData::wait;
    turnPid.sensVal = odom.getT();
    turnPid.target = angle;
    int pwr = clamp((int)turnPid.update(), -g_pidTurnLimit, g_pidTurnLimit);
    printf("%d\n", pwr);
    setDL(-pwr);
    setDR(pwr);
    return turnPid.doneTime + wait < millis();
}
bool pidFace(bool back, int h){
  using turnData::face;
  if (!back){
    turnPid.sensVal = odom.getT() + 2*h*PI;
  } else {
    turnPid.sensVal = odom.getT() + PI + 2*h*PI;
  }
  Point tDir = face - odom.getPos();
  double aErr = acos(tDir.x / tDir.mag());
  if (tDir.y <= 0) aErr = 2*PI - aErr;
  turnPid.target = aErr;
  int pwr = clamp ((int)turnPid.update(), -g_pidTurnLimit, g_pidTurnLimit);
  printf("%d\n", pwr);
  setDL(-pwr);
  setDR(pwr);
  return turnPid.doneTime + turnData::wait < millis();
}
