#ifndef SETUP_H
#define SETUP_H
#include "main.h"
#include "pid.hpp"

extern pros::Motor horiAdjust, vertAdjust, backLeft, backRight, frontRight, frontLeft, shooter, BIntake;
extern pros::Controller c1, c2;
extern pros::ADIEncoder bMWheel, mMWheel;
extern pros::ADIPotentiometer hAngle, vAngle;
extern pros::ADILineSensor bottomBall, topBall;
extern const int BIL, MIL;
extern const double PI, puncherVelocity, puncherHeight, g;

extern const fTarget flagLocation[3][3];

extern const double countsPerInch, countsPerDegree, distBetweenWheel, odomCountPerInch, distanceBackWheel, distanceMiddleWheel;
int clamp(int n, int a, int b);
double clamp(double n, double a, double b);
int millis();

/////////////Drive Functions/////////////
double getDR();
double getDL();
double getMWheel();
double getBWheel();
void setDL(int s);
void setDR(int s);
double getPuncher();
bool pidPuncher(int target);

////Intake Functions////
void setIntake(int s);


////shooterf functions//////
double getHoriAngle();
double getVertAngle();
bool pidVert(double target);
bool pidHori(double target);
void setShooter(int s);

void setConstants();
#endif
