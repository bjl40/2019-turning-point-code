#include "main.h"
#include "pid.hpp"
#include "motors.hpp"
#include "autonomous.hpp"
/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
//6 click
void opcontrol() {

	int row = 2;
	int col = 1;
	static bool xPress = false;
	static bool x1Press = false;
	static double liftTarget = 0;
	static bool aPress = false;
	static bool yPress = false;
	static double timePress = 0;
	int puncherTarget = getPuncher();
	int numPress = 0;
  bool stop = false;
  bool check = false;
  int counter = 0;
	bool middle = false;
	bool high = false;
	int topLine, bottomLine = -200;
	pros::motor_brake_mode_e brakeType = MOTOR_BRAKE_COAST;
	bool puncher = false;
	double prevDLPower, prevDRPower, currDLPower, currDRPower;
	bool tooManyVariables;
	bool press1, press2, press3, press4, press5, press6 = false;
	setConstants();
	bool autoLoad = false;
	int autoTime = 0;
	int stopDelay = 0;
	int doubleShot = 0;
	bool l1 = false;
	bool rev = false;
	while (true){
		//odom.trackPos();
		//if (pidTurn()){
			//c1.rumble("-");
		//}

		//printf("X Pos: %f\t Y Pos: %f\t T Pos: %f\n", odom.getPos().x, odom.getPos().y, odom.getT());
		if (c2.get_digital(DIGITAL_B)){
			topLine = topBall.get_value();
			bottomLine = bottomBall.get_value();
		}
		if (c2.get_digital(DIGITAL_DOWN) && !rev){
			puncherTarget = getPuncher() + 1000;
			puncherPid.doneTime = BIL;
			rev = true;
		}
		if (!c2.get_digital(DIGITAL_DOWN)){
			rev = false;
		}
		odom.trackPos();
		//Intake
		if ((c1.get_digital(DIGITAL_R2) && !stop) || autoLoad){
			setIntake(127);
		} else if (c1.get_digital(DIGITAL_R1) || stop){
			setIntake(-127);
		} else {
			if (c2.get_digital(DIGITAL_L1)){
				setIntake(127);
			} else {
				setIntake(0);
			}
		}




		timePress++;
		if (c1.get_digital(DIGITAL_UP) && !press1){
			press1 = true;
			numPress++;
			timePress = 0;
		}
		if (!c1.get_digital(DIGITAL_UP)){
			press1 = false;
		}
		if (c1.get_digital(DIGITAL_LEFT) && !press2){
			press2 = true;
			numPress+= 10;
			timePress = 0;
		}
		if (!c1.get_digital(DIGITAL_LEFT)){
			press2 = false;
		}
		if (c1.get_digital(DIGITAL_RIGHT) && !press3){
			press3 = true;
			numPress+= 100;
			timePress = 0;
		}
		if (!c1.get_digital(DIGITAL_RIGHT)){
			press3 = false;
		}

		if (c1.get_digital(DIGITAL_A) && !press4){
			press4 = true;
			numPress+= 1000;
			timePress = 0;
		}
		if (!c1.get_digital(DIGITAL_A)){
			press4 = false;
		}
		if (c1.get_digital(DIGITAL_X) && !press5){
			press5 = true;
			numPress+= 10000;
			timePress = 0;
		}
		if (!c1.get_digital(DIGITAL_X)){
			press5 = false;
		}
		if (c1.get_digital(DIGITAL_Y) && !press6){
			press6 = true;
			numPress+= 100000;
			timePress = 0;
		}
		if (!c1.get_digital(DIGITAL_Y)){
			press6 = false;
		}



		if (timePress > 40 && numPress != 0){
			c1.rumble("-");
			switch (numPress)
			{
				case 1: odom.setPos(70, 85, PI/2); break;
				case 2: odom.setPos(72, 37, PI/2); break;
				case 10: odom.setPos(50, 88, PI/2); break;
				case 20: odom.setPos(52, 37, PI/2); break;
				case 100: odom.setPos(98, 84, PI/2); break;
				case 200: odom.setPos(100, 37, PI/2); break;

				/* blue side*//*

				case 1000: odom.setPos(128, 133, PI/2); break;
				case 10000: odom.setPos(77, 133, PI/2); break;
				case 100000: odom.setPos(32, 133, PI/2); break;
*/
				/* red side*/

				case 1000: odom.setPos(111, 133, PI/2); break;
				case 10000: odom.setPos(62, 133, PI/2); break;
				case 100000: odom.setPos(17,133, PI/2); break;
				default: odom.setPos(0, 50, 0); break;
			}
			numPress = 0;
		}

		pros::lcd::clear_line(1);
		pros::lcd::clear_line(2);
		pros::lcd::clear_line(3);



		char line;
		switch (row){
			case 0: line += *"Bottom "; break;
			case 1: line += *"Middle "; break;
			case 2: line += *"Top "; break;
		}
		switch (col){
			case 0: line += *"Left Flag"; break;
			case 1: line += *"Middle Flag"; break;
			case 2: line += *"Right Flag"; break;
		}

		c2.print(1, 1, &line);

		pidPuncher(puncherTarget);
		if (faceFlag(flagLocation[row][col])){
			c2.rumble("-");
			if (pidPuncher(puncherTarget) && c2.get_digital(DIGITAL_R1) && !puncher){
				puncherPid.doneTime = BIL;
				puncherTarget += 2700;
				puncher = true;
				autoLoad = true;
				autoTime = 0;
			}
		}
		if (!c2.get_digital(DIGITAL_R1)){
			puncher = false;
		}

		if(autoLoad){autoTime++;}
		if (autoTime > 40){
			if (row == 2){row = 1;} else {row = 2;}
		}
		if (autoTime > 150){
			autoLoad = false;
			autoTime = 0;
		}

		if (c2.get_digital(DIGITAL_L2) && !tooManyVariables){
			puncherPid.doneTime = BIL;
			puncherTarget += 2700;
			tooManyVariables = true;
			autoTime = 0;
			autoLoad = true;

		}
		if (!c2.get_digital(DIGITAL_L2)){
			tooManyVariables = false;
		}

		if (c2.get_digital(DIGITAL_LEFT)){
			row = 2;
			col = 0;
		}if (c2.get_digital(DIGITAL_UP)){
			row = 2;
			col = 1;
		}
		if (c2.get_digital(DIGITAL_RIGHT)){
			row = 2;
			col = 2;
		}
		if (c2.get_digital(DIGITAL_Y)){
			row = 1;
			col = 0;
		}
		if (c2.get_digital(DIGITAL_X)){
			row = 1;
			col = 1;
		}
		if (c2.get_digital(DIGITAL_A)){
			row = 1;
			col = 2;
		}

		if (bottomBall.get_value() < bottomLine - 30 && topBall.get_value() < topLine-30 && !stop && check){
			c2.rumble("-.-.-");
			stopDelay = 0;
			check = false;
		}




		if (stopDelay < 10){
			stopDelay++;
		} else if (stopDelay < 150){
			stop = true;
			stopDelay++;
		} else{
			stop = false;
		}

    if (bottomBall.get_value() > bottomLine-30){
      check = true;
    }
		/*if (c2.get_digital(DIGITAL_LEFT)){
			horiAdjust.move(-100);
		} else if (c2.get_digital(DIGITAL_RIGHT)){
			horiAdjust.move(100);
		} else {
			horiAdjust.move(0);
		}

		if (c2.get_digital(DIGITAL_UP)){
			vertAdjust.move(57);
		} else if (c2.get_digital(DIGITAL_DOWN)){
			vertAdjust.move(-57);
		} else {
			vertAdjust.move(1);
		}*/

		//shooter
		/*if (c1.get_digital(DIGITAL_L1)){
			setShooter(127);
		} else {
			setShooter(0);
		}*/



		//Drive

		if (c1.get_digital(DIGITAL_B) &&  !x1Press){
			x1Press = true;
			if (brakeType == MOTOR_BRAKE_COAST){brakeType = MOTOR_BRAKE_HOLD;} else {brakeType = MOTOR_BRAKE_COAST;}
		} else if (!c1.get_digital(DIGITAL_B)){
			x1Press = false;
		}

		/*currDLPower = clamp(dir*(c1.get_analog(ANALOG_LEFT_Y) + c1.get_analog(ANALOG_RIGHT_X)), prevDLPower-10, prevDLPower + 10);
		currDRPower = clamp(dir*(c1.get_analog(ANALOG_LEFT_Y) - c1.get_analog(ANALOG_RIGHT_X)), prevDRPower-10, prevDRPower + 10);*/

		currDLPower = (c1.get_analog(ANALOG_LEFT_Y) + c1.get_analog(ANALOG_RIGHT_X));
		currDRPower = (c1.get_analog(ANALOG_LEFT_Y) - c1.get_analog(ANALOG_RIGHT_X));
		if (abs(c1.get_analog(ANALOG_LEFT_Y)) <= 5 && abs(c1.get_analog(ANALOG_RIGHT_X)) <=5 ){
			backLeft.set_brake_mode(brakeType);
			frontLeft.set_brake_mode(brakeType);
			frontRight.set_brake_mode(brakeType);
			backRight.set_brake_mode(brakeType);
		}
			setDL(currDLPower);
			setDR(currDRPower);

		prevDLPower = currDLPower;
		prevDRPower = currDRPower;


		pros::delay(10);

	}

}
