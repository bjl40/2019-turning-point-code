#ifndef PID_H
#define PID_H

class fTarget{
  public:
    double x, y, z;
    fTarget();
    fTarget(double x, double y, double z);
    void setFTarget(double x, double y, double z);
    double mag() const;
    double getZ();
};

class Point{
  public:
    double x, y, t;
    Point();
    Point(double x, double y);
    void setPoint(double x, double y);
    friend Point operator+(const Point& p1, const Point& p2);
    friend Point operator-(const Point& p1, const Point& p2);
    friend double operator*(const Point& p1, const Point& p2);
    friend bool operator>(const Point& p1, const Point& p2);
    friend bool operator<(const Point& p1, const Point& p2);
    double mag() const;
    Point abs();
    Point rotate(int dir) const;
};

class Odom{
  public:
    double iMC, iBC, dC, iLeft, iRight, dLeft, dRight, dCenter, dTheta, dx, dy;
    double z, pT, relativePT;
    double x, y, t, vx, vy;
    double prevTime;
    Odom();
    void trackPos();
    Point getPos();
    void setPos(double x, double y, double t);
    double getT();
    void setT(int t);
};

class PID{
  public:
    double target, DONE_ZONE, sensVal, prevSensVal, iActiveZone, maxIntegral, unwind, prevErr, errTot, kp, ki, kd, deriv;
    int prevTime, doneTime, prevDUpdateTime;
    PID();
    double update();
};

void pidDriveInit(Point target, const int wait);
bool pidDrive(int speed);

bool faceFlag(fTarget flag);
void pidTurnInit(double angle, const int wait);
void pidFaceInit(Point target, const int wait);
bool pidTurn();
bool pidFace(bool back, int dir);
extern PID DLPid, DRPid, drivePid, turnPid, curvePid;
extern PID horiPid, vertPid, puncherPid;
extern Odom odom;

#endif
